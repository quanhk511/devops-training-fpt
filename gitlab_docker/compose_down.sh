#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
set -a
    source "$SH/cnf.env"

    docker-compose -f "$SH/docker-compose.yml"  down

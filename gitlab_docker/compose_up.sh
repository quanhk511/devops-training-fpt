#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
set -a
    source "$SH/cnf.env"

    docker-compose -f "$SH/docker-compose.yml"  up  -d 

    docker ps --format 'Name: {{.Names}} | Ports: {{.Ports}} | Status: {{.Status}}'

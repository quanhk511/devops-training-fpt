#!/bin/bash
# href https://docs.gitlab.com/omnibus/settings/configuration.html
SH=$(cd `dirname $BASH_SOURCE` && pwd)
source "$SH/cnf.env"

    sudo docker exec $C_NAME gitlab-ctl stop puma && \
    sudo docker exec $C_NAME gitlab-ctl stop sidekiq && \
    sudo docker exec $C_NAME gitlab-ctl reconfigure && \
    sudo docker exec $C_NAME gitlab-ctl restart

docker ps --format 'Name: {{.Names}} | Ports: {{.Ports}} | Status: {{.Status}}'

#!/usr/bin/env bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)

source "$SH/.env"
mkdir -p $SH/$WORK_DIR

docker run  -v /var/run/docker.sock:/var/run/docker.sock \
            -v $(which docker):$(which docker) \
            --privileged \
            -v $SH/$WORK_DIR:/var/jenkins_home  \
            -p $PORT:8080  \
            --user 0:0 \
            --name $C_NAME \
            --net gitlab_docker_ci-cd --ip 10.29.0.3 `# assign network gitlab` \
            -d $IMAGE 

    docker ps --format 'Name: {{.Names}} | Ports: {{.Ports}} | Status: {{.Status}}'

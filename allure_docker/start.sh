#!/usr/bin/env bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)

source "$SH/.env"
mkdir -p $SH/$WORK_DIR

docker run  \
            -v $SH/$WORK_DIR:/app/default-reports  \
            -v $SH/$WORK_DIR_1:/app/allure-results  \
            -e CHECK_RESULTS_EVERY_SECONDS=3 -e KEEP_HISTORY=5 \
            -e URL_PREFIX="/prject-fresher" \
            -e EMAILABLE_REPORT_CSS_CDN="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/sketchy/bootstrap.css" \
            -e EMAILABLE_REPORT_TITLE="ALLURE FSOFT" \
            -e CHECK_RESULTS_EVERY_SECONDS=NONE \
            -p $PORT:5050  \
            --user 0:0 \
            --name $C_NAME \
            --net gitlab_docker_ci-cd --ip 10.29.0.7 `# assign network gitlab` \
            -d $IMAGE 

    docker ps --format 'Name: {{.Names}} | Ports: {{.Ports}} | Status: {{.Status}}'

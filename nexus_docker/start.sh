#!/usr/bin/env bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)

source "$SH/.env"
mkdir -p $SH/$WORK_DIR

docker run  \
            -v $SH/$WORK_DIR:/nexus-data  \
            -p $PORT:8081  \
            --name $C_NAME \
            --net gitlab_docker_ci-cd --ip 10.29.0.4 `# assign network gitlab` \
            -d $IMAGE 

    docker ps --format 'Name: {{.Names}} | Ports: {{.Ports}} | Status: {{.Status}}'

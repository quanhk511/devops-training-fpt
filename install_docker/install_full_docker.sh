#!/usr/bin/env bash

`# remove previous version if any`
sudo apt -y remove docker docker-engine docker.io ; 

`# set up apt repository`
sudo apt-get update ;
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common gnupg-agent ;
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - ; sudo apt-key fingerprint 0EBFCD88 ;
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" ;

`# install docker`
sudo apt-get update ;
sudo apt-get install -y docker-ce docker-ce-cli containerd.io ;

`# install docker-compose`
latestUrl=`curl -s https://github.com/docker/compose/releases/latest | grep -Eo "(http[^\"]+)"`  `#sample result of this command https://github.com/docker/compose/releases/tag/1.17.1` ;
version=`echo "$latestUrl" | cut -d '/' -f8`  `#bash split string and get nth element ref. https://unix.stackexchange.com/a/312281/17671` ;
echo $version ;

curl -L "https://github.com/docker/compose/releases/download/$version/docker-compose-`uname -s`-`uname -m`" > ./docker-compose;
sudo mv ./docker-compose /usr/bin/docker-compose;
sudo chmod +x /usr/bin/docker-compose;
  
sudo usermod -aG docker $USER;
newgrp docker ;  `# register new group :docker so as to dodge re-login/restart ref. https://stackoverflow.com/a/48957722/248616`
                 `#TODO this newgrp command will log in new shell! Think about the workaround for it`

sleep 6 ; `# wait sometimes to get thing ready`
docker --version; docker-compose --version;